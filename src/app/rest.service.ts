import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private cliente: HttpClient) { }

  getListadoEquipos(){
    return this.cliente.get('https://mnqy75unx6.execute-api.us-east-1.amazonaws.com/PruebasDemo/equipos');
  };

}
