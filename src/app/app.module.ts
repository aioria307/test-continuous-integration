import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { EquiposComponent } from './coponents/equipos/equipos.component';
import { FiltrosComponent } from './components/filtros/filtros.component';
import { DescriEquipoComponent } from './descri-equipo/descri-equipo.component';
import { EquipoService } from './equipo.service';

@NgModule({
  declarations: [
    AppComponent,
    EquiposComponent,
    FiltrosComponent,
    DescriEquipoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    AppRoutingModule
  ],
  providers: [EquipoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
