import { Component, OnInit } from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {FlatTreeControl} from '@angular/cdk/tree';
import { RestService } from 'src/app/rest.service';
import { stringify } from '@angular/compiler/src/util';
import { TipoComputadora } from 'src/app/Modelo/TipoComputadora';


interface FoodNode {
  TipoComputadora: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    TipoComputadora: 'Fruit',
    children: [
      {TipoComputadora: 'Apple'},
      {TipoComputadora: 'Banana'},
      {TipoComputadora: 'Fruit loops'},
    ]
  }, {
    TipoComputadora: 'Vegetables',
    children: [
      {
        TipoComputadora: 'Green',
        children: [
          {TipoComputadora: 'Broccoli'},
          {TipoComputadora: 'Brussel sprouts'},
        ]
      }, {
        TipoComputadora: 'Orange',
        children: [
          {TipoComputadora: 'Pumpkins'},
          {TipoComputadora: 'Carrots'},
        ]
      },
    ]
  },
];

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-filtros',
  templateUrl: './filtros.component.html',
  styleUrls: ['./filtros.component.css']
})
export class FiltrosComponent implements OnInit {

  // tipComputadora: TipoComputadora[] = [];
  tipComputadora: any;
  equipoPC: any;

  // private _transformer = (node: FoodNode, level: number) => {
  //   return {
  //     expandable: !!node.children && node.children.length > 0,
  //     name: node.TipoComputadora,
  //     level: level,
  //   };
  // }

  // treeControl = new FlatTreeControl<ExampleFlatNode>(
  //     node => node.level, node => node.expandable);

  // treeFlattener = new MatTreeFlattener(
  //     this._transformer, node => node.level, node => node.expandable, node => node.children);

  // dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(public listaequipos: RestService) { 
    // this.dataSource.data = TREE_DATA;
  }

  // hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  ngOnInit(){
    this.listaequipos.getListadoEquipos()
    .subscribe(
      equipos => {
      this.equipoPC = equipos;
      console.log(this.equipoPC)
    },
      err => console.log(err)
    )
  }

}
