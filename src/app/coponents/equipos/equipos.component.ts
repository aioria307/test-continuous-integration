import { Component, OnInit, Inject } from '@angular/core';
import { RestService } from '../../rest.service';
import { TipoComputadora } from 'src/app/Modelo/TipoComputadora';
import { Router, ActivatedRoute } from '@angular/router';
import { EquipoPC } from '../../Modelo/equipo';
import { EquipoService } from '../../equipo.service'
import { Observable } from  'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';

interface FoodNode {
  name: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: 'Fruit',
    children: [
      {name: 'Apple'},
      {name: 'Banana'},
      {name: 'Fruit loops'},
    ]
  }, {
    name: 'Vegetables',
    children: [
      {
        name: 'Green',
        children: [
          {name: 'Broccoli'},
          {name: 'Brussel sprouts'},
        ]
      }, {
        name: 'Orange',
        children: [
          {name: 'Pumpkins'},
          {name: 'Carrots'},
        ]
      },
    ]
  },
];

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.component.html',
  styleUrls: ['./equipos.component.css']
})
export class EquiposComponent implements OnInit {
  ListaEquipo: any;
  equipo2: EquipoPC[] = [];
  array = [];
  dataExt: any[] = [];

  private _transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);

dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  constructor(public listaequipos: RestService,
    private paginacion:Router,
    private route: ActivatedRoute,
    private equiposervice: EquipoService
    ) { 
      this.dataSource.data = TREE_DATA;
  }

  ngOnInit(){
    this.listaequipos.getListadoEquipos()
    .subscribe(
      (equipos:any) => {

      this.ListaEquipo = equipos; // aqui trae toda el json completo
      this.equipo2 = equipos;
      let data:any = equipos as []; 
      let ordenado = data.map( // aqui solo selecciona los campos deseados
        g => {// mapping 
          //console.log("valor g", g)
          return {
          //grouped them by name
            id: g.id, // using lodash to sum quantity
            TipoComputadora: g.TipoComputadora, // using lodash to sum price
          }
        })
        console.log("respuesta", ordenado)
  
        ///
        var result = this.groupBy(ordenado, function(item)
        {
          return [item.TipoComputadora];
        });
        console.log("respuesta group", result)
        let acomodados = [];
        for(let i = 0; i < result.length; i++){
          acomodados.push(result[i][0])
        }
        console.log("acomodados", acomodados)
        console.log("test!!!!!!!!!!!!!")
    },
      err => console.log(err)
    )



  }
  groupBy( array , f )
  {
    var groups = {};
    array.forEach( function( o )
    {
      var group = JSON.stringify( f(o) );
      groups[group] = groups[group] || [];
      groups[group].push( o );  
    });
    return Object.keys(groups).map( function( group )
    {
      return groups[group]; 
    })
  }


  DescripcionEquipo(e){
    this.paginacion.navigate(['equipo/descripcion/', e.id]);
    // console.log(id);
    // this.equipo2 = this.ListaEquipo;

    localStorage.setItem("Detalle", JSON.stringify((e)));  
  }

}