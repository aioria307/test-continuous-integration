import { EquipoService } from './../equipo.service';
import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EquipoPC } from '../Modelo/equipo';

@Component({
  selector: 'app-descri-equipo',
  templateUrl: './descri-equipo.component.html',
  styleUrls: ['./descri-equipo.component.css']
})
export class DescriEquipoComponent implements OnInit {
  panelOpenState = false;
  panelOpenDescription = true;
  Equipo: EquipoPC[] = [];
  id: any;
  DescripcionEquipo: any;

  constructor(private paginacion:Router,
    private route: ActivatedRoute,
    private Equiposeleccionado: EquipoService
    ) {
   }

  ngOnInit() {
    
    // this.Equipo = this.route.snapshot.data[];
    // this.id = stringify(this.route.snapshot.params["id"]); 
    // console.log(this.id);
    let detalle = localStorage.getItem("Detalle");
    this.DescripcionEquipo = JSON.parse(detalle);
    // console.log(this.DescripcionEquipo);
    localStorage.removeItem('Detalle');
  }

}
