import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { RestService} from './rest.service'
import { EquipoPC } from './Modelo/equipo'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TiendaLiverpool';

  constructor(public listaequipos: RestService){}

}
